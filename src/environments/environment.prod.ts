import { URL_WEB } from 'src/app/config/config.model';

export const environment = {
  production: true,
  apiUrl: `${ URL_WEB }`
};
