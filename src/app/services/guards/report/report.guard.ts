// CORE IMPORTS
import { Injectable } from '@angular/core';

// ROUTER IMPORTS
import { CanActivate, Router } from '@angular/router';

// SERVICE IMPORTS
import { OrderService } from '../../order/order.service';

// MODEL IMPORTS
import { Order } from '../../../models/order.model';

@Injectable({
  providedIn: 'root'
})
export class ReportGuard implements CanActivate {

  userOrders: Order[] = [];

  constructor(private _orderService: OrderService,
              private router: Router) {
  }

  canActivate(): boolean{

    this.userOrders = this._orderService.orders;

    if (this.userOrders.length === 0) {
      this.router.navigate(['/home']);
      return false;
    }

    return true;
  }
}
