// CORE IMPORTS
import { Injectable } from '@angular/core';

// ROUTER IMPORTS
import { CanActivate, Router } from '@angular/router';

// SERVICES IMPORTS
import { UserService } from '../../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(public _userService: UserService,
              public router: Router) {}

  canActivate(): boolean {

    if ( !this._userService.isLogged() ) {
      this.router.navigate(['/login']);
      return false;
    }

    return true;
  }

}
