// CORE IMPORTS
import { Injectable } from '@angular/core';

// COMMON IMPORTS
import { HttpClient } from '@angular/common/http';

// RXJS IMPORTS
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

// Constants
import { URL_API } from 'src/app/config/config.model';

// Models
import { Order } from 'src/app/models/model.index';

// Services
import { UserService } from '../user/user.service';
import { EventService } from '../event/event.service';
import { SeatService } from '../seat/seat.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  orders: Order[] = [];

  url: string;

  constructor(private http: HttpClient,
              private userService: UserService,
              private eventService: EventService,
              private seatService: SeatService) {
  this.getOrders();
  console.log(this.userService.userTokens.access_token);
}

createOrder(numbered: boolean, selected: any) {
  this.url = `${ URL_API }/orders?access_token=${ this.userService.userTokens.access_token }`;

  let seatId: number = selected.id;
  let newOrder: any;
  let promise;

  if (!numbered) {

    newOrder = {
      order: {
        name: 'Rubén',
        lastname: 'Juan Molina',
        documentId: '12345678W',
        zipcode: '03660',
        lines: [{
          ticket: selected.id,
          quantity: selected.quantity
        }]
      }
    };
  } else {

    newOrder = {
      order: {
        name: 'Rubén',
        lastname: 'Juan Molina',
        documentId: '12345678W',
        zipcode: '03660',
        lines: [{
          ticket: this.seatService.ticket.id,
          quantity: 1,
          seat: seatId
        }]
      }
    };

  }

  this.http.post(this.url, newOrder)
                            .subscribe( resp => {
                              promise = new Promise( (resolve, reject) => {
                                if (resp) {
                                  resolve(resp);
                                } else {
                                  reject(resp);
                                }
                              }).then((resp: Order) => {
                                this.orders.push(resp);
                              })
                              .catch(err => console.log(err));
                            });
    
}

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.url)
                          .pipe(
                            map( (orders: Order[]) => this.orders = orders),
                            catchError( error => this.getError(error)));
  }

  getError(error: any): Observable<any> {
    console.log(error);
    return throwError(error);
  }

  getUserOrders(): Order[] {
    return this.orders || [];
  }
}
