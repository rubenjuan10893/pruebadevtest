// CORE IMPORTS
import { Injectable } from '@angular/core';

// SERVICES
import { SvgIconRegistryService } from 'angular-svg-icon';

// MODELS
import { SeatmapSeat, Ticket } from 'src/app/models/model.index';

// EXTERNAL IMPORTS
import swal from 'sweetalert';

// ENUM
import { SeatStatus } from 'src/app/config/seatStatus.enum';

@Injectable({
  providedIn: 'root'
})
export class SeatService {

  path: any;
  selected: boolean = false;
  url: string;
  uuidSeat: string;
  ticket: Ticket;

  public selectedSeat: SeatmapSeat = null;
  seatmapSeats: SeatmapSeat[]  = [];
  lastSeat: Element = null;

  constructor(private iconReg: SvgIconRegistryService) { }

  showSeats(ticket: Ticket): boolean {

    // He utilizado para manejar el SVG Vanilla JS para practicar.
    // Falta implementar lo mismo con ViewChild para utilizar Directivas propias de Angular
    if (this.ticket === undefined || this.ticket !== ticket) {
      this.ticket = ticket;
      this.seatmapSeats = ticket.event.seatmap_seats;
      let seatmapFile = ticket.event.seatmap_file;
      seatmapFile = seatmapFile.substring(seatmapFile.lastIndexOf('/') + 1, seatmapFile.length);
  
      this.ticket = ticket;
      
      this.url = `http://localhost:3000/svg/${ seatmapFile }`;
      
      this.iconReg.loadSvg(this.url).subscribe( (resp: SVGAElement) =>  {
  
        // we got the element DOM with the ID
        let svgDiv = document.getElementById('svg');

        // we add an style to the response
        resp.setAttribute('style', 'width: 725px;');
  
        // we get collection from response with class seat
        let seats = Array.from(resp.getElementsByClassName('seat'));
  
        // for each element in collection, we add an eventListener
        // we add to the response
        seats = this.checkSeats(seats);
  
        if (this.checkSeats(seats)) {
          for (let seat of seats) {
            seat.setAttribute('style', 'cursor: pointer;');
            seat.addEventListener('click', () => {
              this.selectSeat(seat);
              
              if (this.selectedSeat !== null) {
                this.changeSelectColor(seats);
              }
            });
          }
        } 

  
        // we add to response parent with childrens
        svgDiv.appendChild(resp);
  
      });
      
      return true;
    }

    return false;
  }

  selectSeat(seat: Element) {
    this.uuidSeat = seat.getAttribute('id');
    this.uuidSeat = this.uuidSeat.substring(this.uuidSeat.indexOf(':') + 1, this.uuidSeat.length);

    for (let mapSeat of this.seatmapSeats) {
      if (this.uuidSeat === mapSeat.uuid) {
        if (mapSeat.tickets[0] === this.ticket.id) {
          this.selectedSeat = mapSeat;
        } else {
          swal('Ticket not valid for this seat', 'Please, select another seat', 'info');
          return false;
        }
      }
    }

    return true;
  }

  changeSelectColor(seats: Element[]) {
    let uuidSeat: string;
    let seatStatus: any;
    
    for (let mapSeat of this.seatmapSeats) {
      if (mapSeat.tickets[0] === this.ticket.id) {
        for (let seat of seats) {
          uuidSeat = seat.getAttribute('id');
          uuidSeat = uuidSeat.substring(uuidSeat.indexOf(':') + 1, uuidSeat.length);
          seatStatus = this.checkStatus(seat);

          if (seatStatus !== SeatStatus.DISABLED) {
            if (uuidSeat === this.selectedSeat.uuid && seatStatus === SeatStatus.FREE) {
              seat.firstElementChild.setAttribute('style', 'fill: red; stroke: red');
              continue;
            } else if (uuidSeat !== this.selectedSeat.uuid || seatStatus === SeatStatus.SELECTED) {
              seat.firstElementChild.setAttribute('style', 'fill: #008000; stroke: #008000;');
              continue;
            }
          }
        }
      } 
    }
  }

  // this is a function that show the available seats for each ticket
  checkSeats(seats: Element[]) {

    let ticketFounded: boolean = false;

    // we comprobate if we have available seats with the selectedTicketID
    for (let validSeat of this.seatmapSeats) {
      if (validSeat.tickets[0] === this.ticket.id){
        ticketFounded = true;
      }
    }

    // if ticket is available
    if (ticketFounded) {
        for (let seat of seats) {
          this.uuidSeat = seat.getAttribute('id');
          this.uuidSeat = this.uuidSeat.substring(this.uuidSeat.indexOf(':') + 1, this.uuidSeat.length);
        
          this.seatmapSeats.forEach(mapSeat => {
            if (this.uuidSeat === mapSeat.uuid) {
              if (this.ticket.id === mapSeat.tickets[0]) {
                seat.firstElementChild.setAttribute('style', 'fill: #008000; stroke: #008000;');
              } else {
                seat.firstElementChild.setAttribute('style', 'fill: grey; stroke: grey;');
              }
            }
          });
        }

        return seats;
    } else {
      swal(`There're not seats availables to the ticket: ${ this.ticket.name }`, 'Try with another Ticket', 'info');

      return [];
    }
  }

  checkStatus(seat: Element): SeatStatus {
    let splitSeatColor = seat.firstElementChild.getAttribute('style').trim().split(';');
    splitSeatColor.splice(splitSeatColor.indexOf(' '), 1);

    for (let color of splitSeatColor) {
      splitSeatColor.splice(splitSeatColor.indexOf(color), 1, color.split(': ')[1]);
    }

    if (splitSeatColor[0] === 'red') {
      return SeatStatus.SELECTED;
    } else if (splitSeatColor[0] === '#008000') {
      return SeatStatus.FREE;
    } else if (splitSeatColor[0] === 'grey') {
      return SeatStatus.DISABLED;
    }
  }
}
