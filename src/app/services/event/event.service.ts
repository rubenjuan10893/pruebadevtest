// CORE IMPORTS
import { Injectable, EventEmitter } from '@angular/core';

// ROUTER IMPORT
import { Router } from '@angular/router';

// EXTERNAL IMPORTS
import swal from 'sweetalert';

// rxjs
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

// Services
import { UserService } from '../user/user.service';
import { HttpClient } from '@angular/common/http';

// Models
import { URL_API } from 'src/app/config/config.model';
import { Event } from 'src/app/models/event.model';
import { Ticket } from 'src/app/models/ticket.model';
import { Authorization, Order } from '../../models/model.index';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  userTokens: Authorization;
  public notification = new EventEmitter<any>();
  
  constructor(public _userService: UserService,
              public http: HttpClient,
              public router: Router) {
    this.userTokens = this._userService.userTokens;
  }

  getEvents(): Observable<Event[]> {
    let url = `${ URL_API }/events?access_token=${ this.userTokens.access_token }`;
    
    return this.http.get<Event[]>(url)
                      .pipe(
                        map( (events: Event[]) => events ),
                        catchError( error => this.getError(error)));
  }

  getEventInfo(id: number): Observable<Event> {

      let url = `${ URL_API }/events/${ id }?access_token=${ this.userTokens.access_token }`;
  
      return this.http.get<Event>(url)
                        .pipe(
                          map( (event: Event) => event ),
                          catchError( error => this.getError(error)));
  }

  getEventTickets(id: number): Observable<Ticket[]> {
    let url = `${ URL_API }/events/${ id }/tickets?access_token=${ this.userTokens.access_token }`;

    return this.http.get<Ticket[]>(url)
                      .pipe(
                        map((tickets: Ticket[]) => tickets),
                        catchError( error => this.getError(error)));
  }

  getError(error: any): Observable<any> {
    console.log(error);
    if (error.status === 401) {
      swal('Session expired', 'Login again. Thank`s!', 'warning');

      this._userService.logout();
      this.notification.emit(error.status);

      return throwError(error.error.error_description);
    }

    if (error.status === 404) {
      this.router.navigate(['/home']);
      return throwError(error.status);
    }

    return throwError(error);
  }
}
