// CORE IMPORTS
import { Injectable } from '@angular/core';

// MODEL IMPORTS
import { Order, Event } from '../../models/model.index';

// SERVICES IMPORTS
import { EventService } from '../event/event.service';
import { OrderService } from '../order/order.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  events: Event[];
  
  userOrders: Order[] = [];
  userOrdersFilteredsByEvent: Order[] = [];
  userOrdersFilteredsByMonth: Order[] = [];

  constructor(public _eventService: EventService,
              public _orderService: OrderService) {
    this.getEvents();
    this.getUserOrders();
  }

  getEvents() {
    this._eventService.getEvents().subscribe(events => this.events = events);
  }

  getUserOrders() {
    this.userOrders = this._orderService.getUserOrders();
  }

  selectEvent(eventSelectedToFilter: string) {
    if (eventSelectedToFilter !== 'null') {
      this.userOrdersFilteredsByEvent = [];
  
      for (let order of this.userOrders) {
        if (eventSelectedToFilter === order.lines[0].ticket.event.name) {
          this.userOrdersFilteredsByEvent.push(order);
        }
      }
  
      if (this.userOrdersFilteredsByEvent.length > 0) {
        return this.userOrdersFilteredsByEvent;
      } else {
        return [];
      }
    } else {
      return this.userOrders;
    }
  }

  selectMonth(monthSelectedToFilter: string) {
    if (monthSelectedToFilter !== 'null') {
      this.userOrdersFilteredsByMonth = [];

      let dateSelectedMonth = monthSelectedToFilter.split('-');
    
      for (let order of this.userOrders) {
        console.log(order);
        let dateEventOrder = order.created_at.split('T');
        dateEventOrder = dateEventOrder[0].split('-');
  
        if (dateEventOrder[0] === dateSelectedMonth[0] && dateEventOrder[1] === dateSelectedMonth[1]) {
          this.userOrdersFilteredsByMonth.push(order);
        }
      }

      if (this.userOrdersFilteredsByMonth.length > 0) {
        return this.userOrdersFilteredsByMonth;
      } else {
        return [];
      }
    } else {
      return this.userOrders;
    }
  }

  orderElements(dataSource: Order[], id: string, _arrowDirection: string) {
    console.log(dataSource);
    let orderDataSource: Order[] = dataSource;
    let firstElementSplit: string[] = [];
    let nextElementSplit: string[] = [];

    for (let f = 0; f < dataSource.length - 1; f++) {
      for (let n = f + 1; n < dataSource.length; n++) {
        firstElementSplit = dataSource[f].lines[0].ticket.event.name.split(' ');
        nextElementSplit = dataSource[n].lines[0].ticket.event.name.split(' ');
        if ( id === 'event_name') {
          if ( _arrowDirection === 'asc') {
            if (firstElementSplit[0].charCodeAt(0) === nextElementSplit[0].charCodeAt(0)) {
              if (firstElementSplit[1].charCodeAt(0) > nextElementSplit[1].charCodeAt(0)) {
                this.moveElement(f, n, orderDataSource);
              }
            }
          } else if (_arrowDirection === 'desc') {
            if (firstElementSplit[0].charCodeAt(0) === nextElementSplit[0].charCodeAt(0)) {
              if (firstElementSplit[1].charCodeAt(0) < nextElementSplit[1].charCodeAt(0)) {
                this.moveElement(f, n, orderDataSource);
              }
            }
          }
        } else if ( id === 'quantity') {
          if (_arrowDirection === 'asc') {
            if (orderDataSource[f].lines[0].quantity > orderDataSource[n].lines[0].quantity) {
              this.moveElement(f, n, orderDataSource);
            }
          } else if (_arrowDirection === 'desc') {
            if (orderDataSource[f].lines[0].quantity < orderDataSource[n].lines[0].quantity) {
              this.moveElement(f, n, orderDataSource);
            }
          }
        } else if (id === 'created_at') {
          let firstDateSplit = orderDataSource[f].created_at.split('T');
          firstDateSplit = firstDateSplit[0].split('-');

          let nextDateSplit = orderDataSource[f + 1].created_at.split('T');
          nextDateSplit = nextDateSplit[0].split('-');
          
          if (_arrowDirection === 'asc') {

            if (firstDateSplit[0] > nextDateSplit[0]) {
              this.moveElement(f, n, orderDataSource);
            } else if (firstDateSplit[1] > nextDateSplit[1]) {
              this.moveElement(f, n, orderDataSource);
            } else if (firstDateSplit[2] > nextDateSplit[2]) {
              this.moveElement(f, n, orderDataSource);
            }

          } else if (_arrowDirection === 'desc') {
            if (firstDateSplit[0] < nextDateSplit[0]) {
              this.moveElement(f, n, orderDataSource);
            } else if (firstDateSplit[1] < nextDateSplit[1]) {
              this.moveElement(f, n, orderDataSource);
            } else if (firstDateSplit[2] < nextDateSplit[2]) {
              this.moveElement(f, n, orderDataSource);
            }
          }
        }
      }
    }

    return orderDataSource;
  }

  moveElement(first: number, next: number, orderDataSource: Order[]) {
    let majorElement = orderDataSource[first];
    orderDataSource.splice(first, 1, orderDataSource[next]);
    orderDataSource.splice(next, 1, majorElement);
  }

  getMonth(numberOfMonth: string) {
    let month: string;

    if (numberOfMonth === '01') {
      month = 'January';
    } else if (numberOfMonth === '02') {
      month = 'February';
    }  else if (numberOfMonth === '03') {
      month = 'March';
    }  else if (numberOfMonth === '04') {
      month = 'April';
    }  else if (numberOfMonth === '05') {
      month = 'May';
    }  else if (numberOfMonth === '06') {
      month = 'June';
    }  else if (numberOfMonth === '07') {
      month = 'July';
    }  else if (numberOfMonth === '08') {
      month = 'August';
    }  else if (numberOfMonth === '09') {
      month = 'September';
    }  else if (numberOfMonth === '10') {
      month = 'October';
    }  else if (numberOfMonth === '11') {
      month = 'November';
    }  else if (numberOfMonth === '12') {
      month = 'December';
    }

    return month;
  }
}
