// CORE IMPORTS
import { NgModule } from '@angular/core';

// COMMON IMPORTS
import { CommonModule } from '@angular/common';

// Services
import { UserService, EventService } from './services.index';

@NgModule({
  declarations: [],
  providers: [
    UserService,
    EventService
  ],
  imports: [
    CommonModule
  ]
})
export class ServicesModule { }
