// Core IMPORTS
import { Injectable, EventEmitter } from '@angular/core';

// ROUTER IMPORTS
import { Router } from '@angular/router';

// Models
import { User } from '../../models/user.model';

// Configuration
import { URL_API } from '../../config/config.model';

// RxJs
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

// Services
import { HttpClient } from '@angular/common/http';
import * as bcrypt from 'bcryptjs';
import swal from 'sweetalert';
import { Authorization } from '../../models/authorization.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  userTokens: Authorization;
  refreshToken: string;
  remember: boolean;

  notificationLogin = new EventEmitter<boolean>();

  constructor(public http: HttpClient,
              public router: Router) {
    this.remember = false;
    this.loadStorage();
  }
 

  saveStorage(token: Authorization, user: User) {
    localStorage.setItem('token', JSON.stringify(token));
    localStorage.setItem('user', JSON.stringify(user));
  }
  
  loadStorage() {
    if (localStorage.getItem('token')) {
      this.userTokens = JSON.parse(localStorage.getItem('token'));
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.userTokens = new Authorization();
      this.user = new User();
    }
  }

  login(username: string, password: string, remember?: boolean) {
    let url = `${ URL_API }/oauth/token/user`;

    if (remember) {
      this.remember = true;
    }

    this.user = new User(username, password);

    return this.http.post(url, this.user)
                      .pipe(
                        map( (userTokens: Authorization) => {
                          this.user.password = bcrypt.hashSync(password, 10);
                          
                          if (!localStorage.getItem('token')) {
                            console.log('HE ENTRADO');
                            this.userTokens = userTokens;
                            console.log(this.userTokens);
                            this.saveStorage(this.userTokens, this.user);
                          }

                          swal('Usuario correcto', `Bienvenido ${ this.user.username }`, 'success');

                          this.notificationLogin.emit(true);

                          return this.user;
                        }),
                        catchError( error => {
                          swal('Login error', error.error.error_description, 'error');

                          return throwError(error);
                        }));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');

    this.router.navigate(['/index']);
  }

  isLogged() {
    return (this.userTokens.access_token.length > 5) ? true : false;
  }
}
