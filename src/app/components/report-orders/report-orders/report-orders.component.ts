// CORE IMPORTS
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';

// MODEL IMPORTS
import { Order } from '../../../models/model.index';

// ANGULAR MATERIAL IMPORTS
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, MatSortHeader } from '@angular/material/sort';

// SERVICE IMPORTS
import { ReportService } from '../../../services/report/report.service';

@Component({
  selector: 'app-report-orders',
  templateUrl: './report-orders.component.html',
  styles: [`
    .table-container {
      width: 100%;
      text-align: center;
    }
    table {
      width: 100%;
    }
  `]
})
export class ReportOrdersComponent implements OnInit, OnChanges {

  option: string;

  userOrders: Order[];

  selectedEventName: string;
  selectedMonthName: string;

  userOrdersFilteredByEvent: Order[];
  userOrdersFilteredByMonth: Order[];

  localDate = new Date().getFullYear().toString() + '-' + (((new Date().getMonth() + 1).toString().length === 1) ? '0' + (new Date().getMonth() + 1).toString() : (new Date().getMonth() + 1).toString());

  tableColumnsGroupByEvents: string[] = ['event_name', 'event_date', 'created_by', 'created_at', 'ticket_name', 'quantity', 'price', 'total'];
  tableColumnsGroupByMonths: string[] = ['created_at', 'ticket_name', 'ticket_description'];
  dataSource;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public _reportService: ReportService) {
  }

  ngOnChanges(): void {
    console.log('ha habido un cambio');
  }
  
  
  ngOnInit(): void {
    this.option = 'null';

    this.userOrdersFilteredByEvent = [];
    this.userOrdersFilteredByMonth = [];
    this.userOrders = this._reportService.userOrders;
  
    this.dataSource = new MatTableDataSource(this.userOrders);
  }

  showOrderBy(optionSelected: string) {

    this.option = optionSelected;
    this.selectedEventName = 'null';
    this.selectedMonthName = 'null';
  }

  selectEvent(eventSelectedToFilter: string) {
    this.selectedEventName = eventSelectedToFilter;
    this.userOrdersFilteredByEvent = this._reportService.selectEvent(this.selectedEventName);

    this.dataSource = new MatTableDataSource(this.userOrdersFilteredByEvent);
  }

  selectMonth(monthSelectedToFilter: string) {
    let dateSelectedMonth = monthSelectedToFilter.split('-');
    this.selectedMonthName = this._reportService.getMonth(dateSelectedMonth[1]);

    this.userOrdersFilteredByMonth = this._reportService.selectMonth(this.selectedMonthName);

    this.dataSource = new MatTableDataSource(this.userOrdersFilteredByMonth);
  }

  order(elementSortHeader: MatSortHeader) {
    if (this.dataSource.filteredData.length > 1){
      this.dataSource = new MatTableDataSource(this.orderElements(elementSortHeader.id, elementSortHeader._arrowDirection));
    }
  }

  orderElements(id: string, _arrowDirection: string) {
    return this._reportService.orderElements(this.dataSource.filteredData, id, _arrowDirection);
  }
}
