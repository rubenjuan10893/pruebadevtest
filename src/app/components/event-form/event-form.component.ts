// CORE IMPORTS
import { Component, OnInit, Input, OnChanges } from '@angular/core';

// MODEL IMPORTS
import { Ticket, Event } from 'src/app/models/model.index';

// SERVICE IMPORTS
import { OrderService, EventService } from 'src/app/services/services.index';

// FORM IMPORTS
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-events-form',
  templateUrl: './event-form.component.html',
  styles: []
})
export class EventFormComponent implements OnChanges, OnInit {

  @Input() eventSelected: Event = null;

  myFormTickets: FormGroup;
  
  ticketsNumbered: Ticket[];
  ticketsNotNumbered: Ticket[];
  selectedTicket: Ticket;

  constructor(public _eventService: EventService,
              private _orderService: OrderService) {

    this.ticketsNumbered = [];
    this.ticketsNotNumbered = [];
    this.selectedTicket = null;
  }

  ngOnInit() {
    this.initForm();
  }
  
  ngOnChanges(): void {
    if (this.eventSelected !== undefined && this.myFormTickets !== undefined) {
      
      this.myFormTickets.setValue({ quantity: null});
      this.getEventTickets(this.eventSelected.id);
      
    } else {
      return;
    }
  }

  initForm() {
    this.myFormTickets = new FormGroup({
      quantity: new FormControl(
        this.ticketsNumbered.forEach(n => n)
      )
    });
  }

  selectDeselectRadioButtons(ticket?: Ticket, nTickets?: string) {
    let idAux: string;
    let idRadio: number;
    let radioCollection: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');

    for (let i = 0; i < radioCollection.length; i++) {
      idAux = radioCollection[i].getAttribute('id');
      idRadio = Number(idAux.substring(idAux.indexOf('_') + 1, idAux.length));

      if (nTickets !== null) {
        if (this.selectedTicket !== null && idRadio === this.selectedTicket.id) {
          radioCollection[i].disabled = false;
          radioCollection[i].checked = true;
        } else {
          radioCollection[i].disabled = true;
          radioCollection[i].checked = false;
        }
      } else if (idRadio === ticket.id) {
        radioCollection[i].disabled = true;
        radioCollection[i].checked = false;
      }
    }
  }

  resetSelect() {
    let selectCollection: HTMLCollectionOf<HTMLSelectElement> = document.getElementsByTagName('select');
    let idAux: string;
    let idSelect: number;

    for (let i = 0; i < selectCollection.length; i++) {
      idAux = selectCollection[i].getAttribute('id');
      idSelect = (idAux !== null) ? Number(idAux.substring(idAux.indexOf('_') + 1, idAux.length)) : null;

      if (idSelect !== null && idSelect !== this.selectedTicket.id) {
        selectCollection[i].value = null;
      }
    }
  }

  selectTicket(ticket: Ticket) {
    let ticketQuantity = {
      ...ticket,
      quantity: ticket.quantity
    };

    this.selectedTicket = ticketQuantity;
  }

  createOrder() {
    this._orderService.createOrder(false, this.selectedTicket);
  }

  getEventTickets( idEvent: number ) {
    this._eventService.getEventTickets(idEvent)
                        .subscribe( (eventTickets: Ticket[]) => {
                          if (this.eventSelected.numbered) {
                            this.ticketsNumbered = eventTickets;
                          } else {
                            this.ticketsNotNumbered = eventTickets;

                            for (let i = 0; i < this.ticketsNotNumbered.length - 1; i++) {
                              this.ticketsNotNumbered[i].quantity = 0;
                            }
                          }
                        });
  }

  setQuantity( ticket: Ticket, nTickets: string ) {
    let newTicket;
    let nTicketsAux = (nTickets === 'null') ? null : nTickets;

    console.log(nTicketsAux);
    if (nTicketsAux === null) {
      this.selectedTicket = null;
      this.selectDeselectRadioButtons(ticket, nTicketsAux);

    } else if (this.selectedTicket === null) {
      for (let ticketAux of this.ticketsNotNumbered) {
        if (ticket.id === ticketAux.id) {
          newTicket = {
            ...ticket,
            quantity: Number(nTicketsAux)
          };
  
          this.ticketsNotNumbered.splice(this.ticketsNotNumbered.indexOf(ticketAux), 1, newTicket);
          this.selectTicket(newTicket);
          this.selectDeselectRadioButtons();
  
          return true;
        }
      }

    } else {
      this.selectedTicket = null;
      this.setQuantity(ticket, nTicketsAux);
      this.resetSelect();
    }

    return false;
  }

  getQuantity(ticket: Ticket) {
    let numTickets: number[] = [];
    for (let i = ticket.minimum; i <= ticket.maximum; i++) {
      numTickets.push(i);
    }

    return numTickets;
  }

  getUserOrders() {
    this._orderService.getUserOrders();
  }

}
