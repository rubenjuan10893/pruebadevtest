// CORE IMPORTS
import { NgModule } from '@angular/core';

// MODULE IMPORTS
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// COMPONENTS IMPORTS
import { EventSelectorComponent } from './event/event-selector/event-selector.component';
import { EventFormComponent } from './event-form/event-form.component';
import { SeatComponent } from './draw/seat/seat.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ReportOrdersComponent } from './report-orders/report-orders/report-orders.component';


@NgModule({
  declarations: [
    EventSelectorComponent,
    EventFormComponent,
    SeatComponent,
    ReportOrdersComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    MatSortModule,
    MatTableModule,
    MatDatepickerModule
  ],
  exports: [
    EventSelectorComponent,
    EventFormComponent,
    SeatComponent
  ]
})
export class ComponentsModule { }
