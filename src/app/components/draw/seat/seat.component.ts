import { Component, OnInit, Input } from '@angular/core';

// MODELS
import { Ticket } from 'src/app/models/model.index';

// SERVICES
import { SeatService, OrderService } from 'src/app/services/services.index';

@Component({
  selector: 'app-seats',
  templateUrl: './seat.component.html',
  styles: [`
    div svg {
      width: 100%;
    }
  `]
})
export class SeatComponent implements OnInit {

  @Input() ticketsEventNumberedSelected: Ticket[];

  constructor(public seatService: SeatService,
              private orderService: OrderService) { }

  ngOnInit(): void {}

  showSeats(ticket: Ticket) {
    this.seatService.showSeats(ticket);
  }

  createOrder() {
    this.orderService.createOrder(true, this.seatService.selectedSeat);
  }

  getUserOrders() {
    this.orderService.getUserOrders();
  }
}
