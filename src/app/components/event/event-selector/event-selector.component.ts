// CORE IMPORTS
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

// ROUTER IMPORTS
import { Router } from '@angular/router';

// MODELS
import { Event, Ticket, Order } from 'src/app/models/model.index';

// SERVICES
import { UserService, EventService } from 'src/app/services/services.index';

@Component({
  selector: 'app-event-selector',
  templateUrl: './event-selector.component.html',
  styles: [
  ]
})
export class EventSelectorComponent implements OnInit {

  events: Event[] = [];
  
  @Output() eventSelected = new EventEmitter<Event>();
  @Output() eventNotNumbered = new EventEmitter<Event>();

  tickets: Ticket[] = [];
  orders: Order[] = [];
  
  constructor(public _eventService: EventService,
              public _userService: UserService,
              public router: Router) { }

  ngOnInit(): void {
    if (this._userService.user) {
      this._eventService.userTokens = this._userService.userTokens;
      this.getEvents();
    } else {
      this.events = [];
    }
  }

  getEvents() {
    this._eventService.getEvents()
                        .subscribe( (events: Event[]) => this.events = events,
                                    () => this.router.navigate(['/unoauthorized']));
  }

  selectEvent( idEvent: string ) {
    let id = Number(idEvent);
    
    this._eventService.getEventInfo(id)
                        .subscribe( (eventInfo: Event) => this.eventSelected.emit(eventInfo),
                                    () => this.eventSelected.emit(null));
  }
}
