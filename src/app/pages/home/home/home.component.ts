// CORE IMPORTS
import { Component, OnInit } from '@angular/core';

// MODEL IMPORTS
import { Event } from 'src/app/models/model.index';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  event: Event;

  constructor() {}
  ngOnInit() {
  }

  eventSelected( event: Event ) {
    this.event = event;
  }
}
