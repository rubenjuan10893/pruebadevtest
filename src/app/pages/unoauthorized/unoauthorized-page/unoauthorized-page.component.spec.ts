import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnoauthorizedPageComponent } from './unoauthorized-page.component';

describe('UnoauthorizedPageComponent', () => {
  let component: UnoauthorizedPageComponent;
  let fixture: ComponentFixture<UnoauthorizedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnoauthorizedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnoauthorizedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
