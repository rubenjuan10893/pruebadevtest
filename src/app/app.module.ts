import { BrowserModule } from '@angular/platform-browser';

// Modules
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServicesModule } from './services/services.module';
import { ComponentsModule } from './components/components.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { APP_ROUTES } from './app.routes';

// Components
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomeComponent } from './pages/home/home/home.component';

import { HeaderComponent } from './shared/header/header.component';
import { UnoauthorizedPageComponent } from './pages/unoauthorized/unoauthorized-page/unoauthorized-page.component';
import { IndexComponent } from './pages/index/index/index.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent,
    HeaderComponent,
    UnoauthorizedPageComponent,
    IndexComponent
  ],
  imports: [
    ComponentsModule,
    BrowserModule,
    ServicesModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTES,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    BrowserAnimationsModule,
    MatSortModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
