import { Ticket } from './ticket.model';

export interface Line {
    ticket: Ticket;
    quantity: number;
    id?: number;
    uuid?: string;
}