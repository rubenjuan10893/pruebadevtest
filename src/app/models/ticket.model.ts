import { Event } from './model.index';

export interface Ticket {
    description: string;
    event: Event;
    id: number;
    maximum: number;
    minimum: number;
    name: string;
    price: number;
    quantity: number;
}
