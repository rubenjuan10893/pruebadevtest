export interface SeatmapSeat {
    id: number;
    number: number;
    row: number;
    uuid: string;
    x: number;
    y: number;
    tickets?: number[];
}
