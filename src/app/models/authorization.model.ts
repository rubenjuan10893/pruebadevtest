export class Authorization {
    constructor(public access_token: string = '',
                public refresh_token: string = '') {}
}