import { Line } from './line.model';

export interface Order {
    name: string;
    lastname: string;
    documentId: string;
    zipcode: string;
    lines: Line;
    created_at?: string;
    uuid?: string;
    id?: number;
}