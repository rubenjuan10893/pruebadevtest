import { SeatmapSeat } from './model.index';

export interface Event {
    date: string;
    description: string;
    id: number;
    name: string;
    numbered: boolean;
    seatmap_file?: string;
    seatmap_seats?: SeatmapSeat[];
}
