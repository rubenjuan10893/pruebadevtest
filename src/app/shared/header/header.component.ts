// CORE IMPORTS
import { Component, OnInit } from '@angular/core';

// MODEL IMPORTS
import { User, Order } from '../../models/model.index';

// SERVICE IMPORTS
import { UserService, EventService } from '../../services/services.index';
import { OrderService } from '../../services/order/order.service';

// FORM IMPORTS
import { Validators, FormGroup, FormControl } from '@angular/forms';

// ROUTER IMPORTS
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
    div > span {
      margin-right: 15px;
      color: white;
    }
  `]
})
export class HeaderComponent implements OnInit {

  user: User;
  userOrders: Order[];
  headerLoginForm: FormGroup;

  constructor(public _userService: UserService,
              public router: Router,
              public _eventService: EventService,
              public _orderService: OrderService) { }

  ngOnInit(): void {
    this.loadStorage();
    this.initForm();
    
    this._eventService.notification.subscribe( err => {
      if (err.status === 401) {
        console.log(err);
        this.user = null;
      }
    });
    
    this.userOrders = this._orderService.getUserOrders();
  }

  loadStorage() {
    if (localStorage.getItem('token') && localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.user = null;
    }
  }

  initForm() {
    this.headerLoginForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  login() {
    if (this.headerLoginForm.valid) {

      let username = this.headerLoginForm.value.username;
      let password = this.headerLoginForm.value.password;

      this._userService.login(username, password)
                          .subscribe( (userLogged: User) => {
                            this.user = userLogged;
                            this.router.navigate(['/home']);
                          },
                          () => { // If we have some error, we reset the form values and remove the user
                            this.headerLoginForm.reset();
                          });
    }
  }

  logout() {
    this.user = null;
    this.initForm();
    this._userService.logout();
  }

  reportOrders() {
    this.router.navigate(['/report']);
  }

  goHome() {
    this.router.navigate(['/home']);
  }
}
