import { Routes, RouterModule } from '@angular/router';

// Components
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { LoginGuard } from './services/guards/login/login.guard';
import { HomeComponent } from './pages/home/home/home.component';
import { UnoauthorizedPageComponent } from './pages/unoauthorized/unoauthorized-page/unoauthorized-page.component';
import { IndexComponent } from './pages/index/index/index.component';
import { ReportOrdersComponent } from './components/report-orders/report-orders/report-orders.component';
import { ReportGuard } from './services/guards/report/report.guard';

const ROUTES: Routes = [
    {
        path: '',
        redirectTo: '/index',
        pathMatch: 'full'
    }, {
        path: 'index',
        component: IndexComponent
    }, {
        path: 'report',
        component: ReportOrdersComponent,
        canActivate: [ReportGuard]
    }, {
        path: 'home',
        component: HomeComponent,
        canActivate: [LoginGuard]
    }, {
        path: 'page-not-found',
        component: PageNotFoundComponent
    },  {
        path: 'unoauthorized',
        component: UnoauthorizedPageComponent
    }, {
        path: '**',
        redirectTo: '/page-not-found',
        pathMatch: 'full'
    }
];

export const APP_ROUTES = RouterModule.forRoot(ROUTES, { useHash: true });
