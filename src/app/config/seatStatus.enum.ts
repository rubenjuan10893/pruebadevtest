export enum SeatStatus {
    DISABLED = 'disabled',
    FREE = 'free',
    SELECTED = 'selected'
}